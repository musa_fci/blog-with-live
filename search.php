<?php
include './inc/header.php';
?>

<?php
if (!isset($_POST['search']) || $_POST['search'] == NULL) {
    header('location:404.php');
} else {
    $search = $_POST['search'];
}
?>

<div class="contentsection contemplete clear">

    <div class="maincontent clear">

        <?php
        $query = "SELECT * FROM tbl_post WHERE title LIKE '%$search%' OR body LIKE '%$search%' ";
        $post = $obj->select($query);

        if ($post) {
            foreach ($post as $data) {
                ?>
                <div class="samepost clear">
                    <h2>
                        <a href="post.php?id=<?php echo $data['id']; ?>">
                            <?php echo $data['title']; ?>
                        </a>
                    </h2>
                    <h4><?php echo $fm->formatDate($data['date']); ?>, By <a href="#"><?php echo $data['author']; ?></a></h4>
                    <a href="#"><img src="admin/upload/<?php echo $data['image']; ?>" alt="post image"/></a>
                    <p>
                        <?php echo $fm->textShort($data['body'], 350); ?>
                    </p>
                    <div class="readmore clear">
                        <a href="post.php?id=<?php echo $data['id']; ?>">Read More</a>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <p style="color: red;">Your Search Result Not Found...!!!</p>
            <?php
        }
        ?>

    </div>

    <?php
    include './inc/sidebar.php';
    ?>

</div>

<?php
include './inc/footer.php';
