<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Category List</h2>
        <?php
        if (isset($_GET['catId'])) {
            $delid = $_GET['catId'];
            $query = "DELETE FROM tbl_category WHERE category_id='$delid'";
            $msg = $obj->delete($query);
            if ($msg) {
                echo "Category Delete Successfuly....!";
            } else {
                echo "Category Not Delete";
            }
        }
        ?>
        <div class="block">        
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Category Id.</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT * FROM tbl_category ORDER BY category_id asc";
                    $category = $obj->select($query);
                    $i = 0;
                    if ($category) {
                        foreach ($category as $value) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $value['category_id']; ?></td>
                                <td><?php echo $value['category_name']; ?></td>
                                <td><a href="catedit.php?catId=<?php echo $value['category_id']; ?>">Edit</a> || 
                                    <a onclick="return confirm('Are You Sure to Delete !!')" href="?catId=<?php echo $value['category_id']; ?>">Delete</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php
include './inc/footer.php';
?>