<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<div class="grid_10">
    <style type="text/css">
        .left_side{width: 70%;float: left;}
        .right_side{width: 20%;float: right;}
        .right_side img{width: 150px;height: 100px;}
    </style>
    <div class="box round first grid">
        <h2>Update Site Title and Description</h2>
        <?php
        if (isset($_POST['btn'])) {

            $permit = array('png');
            $pic_name = $_FILES['logo']['name'];
            $pic_size = $_FILES['logo']['size'];
            $tmp_name = $_FILES['logo']['tmp_name'];

            $div = explode('.', $pic_name);
            $pic_ext = strtolower(end($div));
            $same_name = 'logo' . '.' . $pic_ext;
            $pic_folder = "upload/" . $same_name;

            if (!empty($pic_name)) {
                if ($pic_size > 1048567) {
                    echo 'Image Size is too Larze';
                } elseif (in_array($pic_ext, $permit) === FALSE) {
                    return "select correct file formate like as:- " . implode(',', $permit);
                } else {
                    move_uploaded_file($tmp_name, $pic_folder);

                    $query = "UPDATE title_slogan SET title='$_POST[title]',slogan='$_POST[slogan]',logo='$same_name' WHERE id = '1'";
                    $postupdate = $obj->update($query);
                    if ($postupdate) {
                        echo 'Data Update Successfully....!';
                    } else {
                        echo 'Data Not Updated.....!';
                    }
                }
            } else {
                $query = "UPDATE title_slogan SET title='$_POST[title]',slogan='$_POST[slogan]' WHERE id = '1'";
                $postupdate = $obj->update($query);
                if ($postupdate) {
                    echo 'Data Update Successfully....!';
                } else {
                    echo 'Data Not Updated.....!';
                }
            }
        }
        ?>





        <?php
        $query = "SELECT * FROM title_slogan WHERE id='1'";
        $data = $obj->select($query);
        if ($data) {
            foreach ($data as $value) {
                ?>
                <div class="block sloginblock">
                    <div class="left_side">
                        <form action="" method="post" enctype="multipart/form-data">
                            <table class="form">					
                                <tr>
                                    <td>
                                        <label>Website Title</label>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $value['title']; ?>" name="title" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Website Slogan</label>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $value['slogan']; ?>" name="slogan" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Website Logo</label>
                                    </td>
                                    <td>
                                        <input type="file" name="logo"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <input type="submit" name="btn" Value="Update" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="right_side">
                        <img src="upload/<?php echo $value['logo']; ?>" alt="">
                    </div>
                </div>
                <?php
            }
        }
        ?>

    </div>
</div>
<?php
include './inc/footer.php';
?>

