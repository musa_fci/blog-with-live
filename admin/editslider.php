<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!isset($_GET['id']) || $_GET['id'] == NULL) {
    echo "<script>window.location='sliderlist.php'</script>";
} else {
    $id = $_GET['id'];
}
?> 



<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Post</h2>
        <?php
        if (isset($_POST['btn'])) {

            $permit = array('jpg', 'png', 'jpeg', 'gif');
            $pic_name = $_FILES['image']['name'];
            $pic_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $pic_name);
            $pic_ext = strtolower(end($div));
            $unique_name = substr(md5(time()), 0, 10) . '.' . $pic_ext;
            $pic_folder = "upload/" . $unique_name;

            if (!empty($pic_name)) {
                if ($pic_size > 1048567) {
                    echo 'Image Size is too Larze';
                } elseif (in_array($pic_ext, $permit) === FALSE) {
                    return "select correct file formate like as:- " . implode(',', $permit);
                } else {
                    move_uploaded_file($tmp_name, $pic_folder);
                    $query = "UPDATE tbl_slider SET title='$_POST[title]',image='$unique_name' WHERE id = '$id'";
                    $postupdate = $obj->update($query);
                    if ($postupdate) {
                        echo 'Slider Update Successfully....!';
                    } else {
                        echo 'Slider Not Updated.....!';
                    }
                }
            } else {
                $query = "UPDATE tbl_slider SET title='$_POST[title]' WHERE id = '$id'";
                $postupdate = $obj->update($query);
                if ($postupdate) {
                    echo 'Slider Update Successfully....!';
                } else {
                    echo 'Slider Not Updated.....!';
                }
            }
        }
        ?>


        <div class="block">

            <?php
            $query = "SELECT * FROM tbl_slider WHERE id = '$id'";
            $slider = $obj->select($query);
            if ($slider) {
                foreach ($slider as $data) {
                    ?>

                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="form">

                            <tr>
                                <td>
                                    <label>Title</label>
                                </td>
                                <td>
                                    <input type="text" name="title" value="<?php echo $data['title']; ?>" class="medium" />
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <label>Upload Image</label>
                                </td>
                                <td>
                                    <img src="upload/<?php echo $data['image']; ?>" width="150px" height="50px">
                                    <br>
                                    <input name="image" type="file" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="Save" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>