<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Inbox</h2>
        <?php
        if (isset($_GET['seenid'])) {
            $id = $_GET['seenid'];
            $query = "UPDATE tbl_contact SET status='1' WHERE id = '$id'";
            $msg = $obj->update($query);
            if ($msg) {
                echo "<span style='color:green'>Mail seen Successfully......!</span>";
            } else {
                echo "<span style='color:red'>Mail Not seen..........!</span>";
            }
        }
        ?>
        <div class="block">        
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message Body</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT * FROM tbl_contact WHERE status='0'";
                    $data = $obj->select($query);
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $value['firstname'] . ' ' . $value['lastname']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $fm->textShort($value['body'], 50); ?></td>
                                <td><?php echo $fm->formatDate($value['date']); ?></td>
                                <td>
                                    <a href="viewmsg.php?msgid=<?php echo $value['id'] ?>">view</a> || 
                                    <a href="replaymsg.php?msgid=<?php echo $value['id'] ?>">replay</a> ||
                                    <a href="?seenid=<?php echo $value['id'] ?>">seen</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>



    <div class="box round first grid">
        <h2>Seen</h2>
        <?php
        if (isset($_GET['delid'])) {
            $delid = $_GET['delid'];
            $query = "DELETE FROM tbl_contact WHERE id='$delid'";
            $msg = $obj->delete($query);
            if ($msg) {
                echo "<span style='color:green'>Message Delete Successfuly....!</span>";
            } else {
                echo "<span style='color:green'>Message Not Delete......!</span>";
            }
        }
        ?>
        <div class="block">        
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message Body</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT * FROM tbl_contact WHERE status='1'";
                    $category = $obj->select($query);
                    $i = 0;
                    if ($category) {
                        foreach ($category as $value) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $value['firstname'] . ' ' . $value['lastname']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $fm->textShort($value['body'], 50); ?></td>
                                <td><?php echo $fm->formatDate($value['date']); ?></td>
                                <td>
                                    <a href="viewmsg.php?msgid=<?php echo $value['id'] ?>">view</a> || 
                                    <a href="?delid=<?php echo $value['id'] ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>



</div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php
include './inc/footer.php';
?>
