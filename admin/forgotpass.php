<?php
include '../lib/session.php';
Session::checkLogin();
?>


<?php
include '../config/config.php';
include '../lib/Database.php';
include '../helpers/format.php';
?>

<?php
$obj = new Database;
$fm = new Format;
?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
    <div class="container">
        <section id="content">
            <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $email = $fm->validation($_POST['email']);

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    echo 'Invalid Email Address';
                } else {
                    $mailquery = "SELECT * FROM tbl_user WHERE email='$email' LIMIT 1";
                    $mailcheck = $obj->select($mailquery);

                    if ($mailcheck != FALSE) {
                        foreach ($mailcheck as $data) {
                            $userid = $data['user_id'];
                            $username = $data['name'];
                        }

                        $text = substr($email, 0, 3);
                        $rand = rand(1000, 9999);
                        $newpass = "$text$rand";
                        $password = md5($newpass);

                        $updatePass = "UPDATE tbl_user SET password='$password' WHERE user_id='$userid'";
                        $update = $obj->update($updatePass);

                        $to = "$email";
                        $form = "musa.fci@gmail.com";
                        $headers = "From: $form\n";
                        $headers .= 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $subject = "Your New Password";
                        $message = "Your User name is" . $username . "And Password is" . $newpass;

                        $sendmail = mail($to, $subject, $message);
                        if ($sendmail) {
                            echo "<span style='color:green;font-size:20px;'>Please Check Your Email For New Password......!!</span>";
                        } else {
                            echo "<span style='color:red;font-size:20px;'>Email Not Send......!!</span>";
                        }
                    } else {
                        echo "<span style='color:red;font-size:20px;'>Email Not Exist....</span>";
                    }
                }
            }
            ?>
            <form action="" method="post">
                <h1>Password Recovery</h1>
                <div>
                    <input type="text" name="email" placeholder="Enter Valid Email Address" required=""/>
                </div>
                <div>
                    <input type="submit" value="send email" />
                </div>
            </form>
            <div class="button">
                <a href="login.php">Login</a>
            </div><!-- button -->
        </section><!-- content -->
    </div><!-- container -->
</body>
</html>