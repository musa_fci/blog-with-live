<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (isset($_GET['delid'])) {

    $id = $_GET['delid'];
    $query = "SELECT * FROM tbl_post WHERE id='$id'";
    $post = $obj->select($query);

    foreach ($post as $value) {
        $img = 'upload/' . $value['image'];
        unlink($img);
    }


    $query = "DELETE FROM tbl_post WHERE id = '$id'";
    $delpost = $obj->delete($query);
    if ($delpost) {
        echo "Post delete  successfully.......!!";
    } else {
        echo 'Post not delete';
    }
}
?>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Post List</h2>

        <div class="block">  
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th width="5%">Id</th>
                        <th width="10%">Title</th>
                        <th width="20%">Description</th>
                        <th width="5%">Cat</th>
                        <th width="20%">Image</th>
                        <th width="10%">Author</th>
                        <th width="10%">Tags</th>
                        <th width="10%">Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT tbl_post.*,tbl_category.category_name FROM tbl_post INNER JOIN tbl_category ON tbl_post.cat_id = tbl_category.category_id ORDER BY tbl_post.id DESC";
                    $post = $obj->select($query);
                    if ($post) {
                        foreach ($post as $value) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $value['title']; ?></td>
                                <td><?php echo $fm->textShort($value['body'], 60); ?></td>
                                <td><?php echo $value['category_name']; ?></td>
                                <td><img src="upload/<?php echo $value['image']; ?>" width="100px" height="100px"></td>
                                <td><?php echo $value['author']; ?></td>
                                <td><?php echo $value['tags']; ?></td>
                                <td><?php echo $fm->formatDate($value['date']); ?></td>
                                <td>
                                    <a href="viewpost.php?id=<?php echo $value['id']; ?>">View</a>

                                    <?php
                                    if (Session::get('userId') == $value['userid'] || Session::get('userRole') == '0') {
                                        ?>
                                        || <a href="editpost.php?id=<?php echo $value['id']; ?>">Edit</a> || 
                                        <a href="?delid=<?php echo $value['id']; ?>">Delete</a></td>
                                <?php } ?>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
            </table>

        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>

<?php
include './inc/footer.php';
?>