<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Post</h2>
        <?php
        if (isset($_POST['btn'])) {

            $permit = array('jpg', 'png', 'jpeg', 'gif');
            $pic_name = $_FILES['image']['name'];
            $pic_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $pic_name);
            $pic_ext = strtolower(end($div));
            $unique_name = substr(md5(time()), 0, 10) . '.' . $pic_ext;
            $pic_folder = "upload/" . $unique_name;

            if (empty($pic_name)) {
                echo 'No Image Select';
            } elseif ($pic_size > 1048567) {
                echo 'Image Size is too Larze';
            } elseif (in_array($pic_ext, $permit) === FALSE) {
                return "select correct file formate like as:- " . implode(',', $permit);
            } else {
                move_uploaded_file($tmp_name, $pic_folder);

                $query = "INSERT INTO tbl_post(cat_id,title,body,image,author,tags,userid)VALUES('$_POST[cat_id]','$_POST[title]','$_POST[body]','$unique_name','$_POST[author]','$_POST[tags]','$_POST[userid]')";
                $postinsert = $obj->insert($query);
                if ($postinsert) {
                    echo 'Post Insert Successfully....!';
                } else {
                    echo 'Post Not Inserted.....!';
                }
            }
        }
        ?>


        <div class="block">               
            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="title" placeholder="Enter Post Title..." class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="cat_id">
                                <option>select category</option>
                                <?php
                                $query = "SELECT * FROM tbl_category";
                                $result = $obj->select($query);
                                if ($result) {
                                    foreach ($result as $value) {
                                        ?>
                                        <option value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <input name="image" type="file" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Content</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tags</label>
                        </td>
                        <td>
                            <input type="text" name="tags" placeholder="Enter Tags Name..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Author</label>
                        </td>
                        <td>
                            <input type="text" readonly="" name="author" value="<?php echo Session::get('username'); ?>" class="medium" />
                            <input type="hidden" readonly="" name="userid" value="<?php echo Session::get('userId'); ?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="btn" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>