<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Page</h2>
        <?php
        if (isset($_POST['btn'])) {
            $query = "INSERT INTO tbl_page(name,body)VALUES('$_POST[name]','$_POST[body]')";
            $msg = $obj->insert($query);
            if ($msg) {
                echo 'Page create successfully';
            } else {
                echo 'Page not create';
            }
        }
        ?>


        <div class="block">               
            <form action="" method="post">
                <table class="form">
                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" name="name" placeholder="Enter Page Name..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Body</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="btn" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>