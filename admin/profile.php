<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
$userid = Session::get('userId');
$userrole = Session::get('userRole');
?>


<div class="grid_10">
    <div class="box round first grid">
        <h2>User Profile</h2>
        <?php
        if (isset($_POST['btn'])) {

            $query = "UPDATE tbl_user SET name='$_POST[name]',username='$_POST[username]',email='$_POST[email]',details='$_POST[details]' WHERE user_id='$userid'";
            $userupdate = $obj->update($query);
            if ($userupdate) {
                echo 'User Update Successfully....!';
            } else {
                echo 'User Not Updated.....!';
            }
        }
        ?>


        <div class="block">

            <?php
            $query = "SELECT * FROM tbl_user WHERE user_id='$userid' && role='$userrole'";
            $user = $obj->select($query);
            if ($user) {
                foreach ($user as $userdata) {
                    ?>

                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="form">

                            <tr>
                                <td>
                                    <label>Name</label>
                                </td>
                                <td>
                                    <input type="text" name="name" value="<?php echo $userdata['name']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>UserName</label>
                                </td>
                                <td>
                                    <input type="text" name="username" value="<?php echo $userdata['username']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Email</label>
                                </td>
                                <td>
                                    <input type="text" name="email" value="<?php echo $userdata['email']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Details</label>
                                </td>
                                <td>
                                    <textarea class="tinymce" name="details">
                                        <?php echo $userdata['details']; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="Save" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>