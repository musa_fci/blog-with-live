<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Slider List</h2>

        <?php
        if (isset($_GET['delid'])) {

            $id = $_GET['delid'];
            $query = "SELECT * FROM tbl_slider WHERE id='$id'";
            $post = $obj->select($query);

            foreach ($post as $value) {
                $img = 'upload/' . $value['image'];
                unlink($img);
            }


            $query = "DELETE FROM tbl_slider WHERE id = '$id'";
            $delpost = $obj->delete($query);
            if ($delpost) {
                echo "Post delete  successfully.......!!";
            } else {
                echo 'Post not delete';
            }
        }
        ?>


        <div class="block">  
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th width="5%">Id</th>
                        <th width="10%">Title</th>
                        <th width="20%">Image</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT * FROM tbl_slider";
                    $slider = $obj->select($query);
                    if ($slider) {
                        foreach ($slider as $value) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $value['title']; ?></td>
                                <td><img src="upload/<?php echo $value['image']; ?>" width="100px" height="100px"></td>
                                <td>

                                    <?php
                                    if (Session::get('userRole') == '0') {
                                        ?>
                                         <a href="editslider.php?id=<?php echo $value['id']; ?>">Edit</a> || 
                                        <a href="?delid=<?php echo $value['id']; ?>">Delete</a></td>
                                <?php } ?>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
            </table>

        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>

<?php
include './inc/footer.php';
?>