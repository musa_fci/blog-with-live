<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!isset($_GET['id']) || $_GET['id'] == NULL) {
    echo "<script>window.location='postlist.php'</script>";
} else {
    $id = $_GET['id'];
}
?> 



<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Post</h2>
        <?php
        if (isset($_POST['btn'])) {

            $permit = array('jpg', 'png', 'jpeg', 'gif');
            $pic_name = $_FILES['image']['name'];
            $pic_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $pic_name);
            $pic_ext = strtolower(end($div));
            $unique_name = substr(md5(time()), 0, 10) . '.' . $pic_ext;
            $pic_folder = "upload/" . $unique_name;

            if (!empty($pic_name)) {
                if ($pic_size > 1048567) {
                    echo 'Image Size is too Larze';
                } elseif (in_array($pic_ext, $permit) === FALSE) {
                    return "select correct file formate like as:- " . implode(',', $permit);
                } else {
                    move_uploaded_file($tmp_name, $pic_folder);
                    $query = "UPDATE tbl_post SET cat_id='$_POST[cat_id]',title='$_POST[title]',body='$_POST[body]',image='$unique_name',author='$_POST[author]',tags='$_POST[tags]',userid='$_POST[userid]' WHERE id = '$id'";
                    $postupdate = $obj->update($query);
                    if ($postupdate) {
                        echo 'Post Update Successfully....!';
                    } else {
                        echo 'Post Not Updated.....!';
                    }
                }
            } else {
                $query = "UPDATE tbl_post SET cat_id='$_POST[cat_id]',title='$_POST[title]',body='$_POST[body]',author='$_POST[author]',tags='$_POST[tags]',userid='$_POST[userid]' WHERE id = '$id'";
                $postupdate = $obj->update($query);
                if ($postupdate) {
                    echo 'Post Update Successfully....!';
                } else {
                    echo 'Post Not Updated.....!';
                }
            }
        }
        ?>


        <div class="block">

            <?php
            $query = "SELECT * FROM tbl_post WHERE id = '$id'";
            $post = $obj->select($query);
            if ($post) {
                foreach ($post as $postValue) {
                    ?>

                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="form">

                            <tr>
                                <td>
                                    <label>Title</label>
                                </td>
                                <td>
                                    <input type="text" name="title" value="<?php echo $postValue['title']; ?>" class="medium" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label>Category</label>
                                </td>
                                <td>
                                    <select id="select" name="cat_id">
                                        <option>select category</option>
                                        <?php
                                        $query = "SELECT * FROM tbl_category";
                                        $result = $obj->select($query);
                                        if ($result) {
                                            foreach ($result as $value) {
                                                ?>
                                                <option 
                                                <?php if ($postValue['cat_id'] == $value['category_id']) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                    value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Upload Image</label>
                                </td>
                                <td>
                                    <img src="upload/<?php echo $postValue['image']; ?>" width="150px" height="50px">
                                    <br>
                                    <input name="image" type="file" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Content</label>
                                </td>
                                <td>
                                    <textarea class="tinymce" name="body">
                                        <?php echo $postValue['body']; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Tags</label>
                                </td>
                                <td>
                                    <input type="text" name="tags" value="<?php echo $postValue['tags']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Author</label>
                                </td>
                                <td>
                                    <input type="text" name="author" value="<?php echo $postValue['author']; ?>" class="medium" />
                                    <input type="hidden" readonly="" name="userid" value="<?php echo Session::get('userId'); ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="Save" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>