<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (isset($_GET['pageid'])) {
    $pageid = $_GET['pageid'];
}
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Page</h2>

        <?php
        if (isset($_POST['btn'])) {
            $query = "UPDATE tbl_page SET name='$_POST[name]',body='$_POST[body]' WHERE id='$pageid'";
            $msg = $obj->update($query);
            if ($msg) {
                echo 'Page update successfully';
            } else {
                echo 'Page not updated';
            }
        }
        ?>

        <div class="block"> 
            <?php
            $query = "SELECT * FROM tbl_page WHERE id='$pageid'";
            $result = $obj->select($query);
            if ($result) {
                foreach ($result as $value) {
                    ?>
                    <form action="" method="post">
                        <table class="form">
                            <tr>
                                <td>
                                    <label>Name</label>
                                </td>
                                <td>
                                    <input type="text" name="name" value="<?php echo $value['name']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Body</label>
                                </td>
                                <td>
                                    <textarea class="tinymce" name="body">
                                        <?php echo $value['body']; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="update" />
                                    <span style="background: red;color: white;padding: 5px;">
                                        <a href="deletepage.php?delid= <?php echo $value['id']; ?>">delete</a>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                }
            }
            ?>
        </div>

    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>