<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Category</h2>
        <div class="block copyblock"> 
            <?php
            if (isset($_POST['btn'])) {
                $data = $_POST['category_name'];

                if (empty($data)) {
                    echo "Field Must not be Empty";
                } else {
                    $query = "INSERT INTO tbl_category (category_name)VALUES('$data')";
                    $inserted = $obj->insert($query);

                    if ($inserted) {
                        echo "Category Inserted Successfully....!";
                    } else {
                        echo "No Category Inserted";
                    }
                }
            }
            ?>

            <form action="" method="post">
                <table class="form">					
                    <tr>
                        <td>
                            <input type="text" name="category_name" placeholder="Enter Category Name..." class="medium" />
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <input type="submit" name="btn" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php
include './inc/footer.php';
?>
