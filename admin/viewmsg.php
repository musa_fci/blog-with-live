<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!isset($_GET['msgid']) || $_GET['msgid'] == NULL) {
    echo "<script>window.location='inbox.php'</script>";
} else {
    $id = $_GET['msgid'];
}
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>View Messages</h2>

        <?php
        if (isset($_POST['btn'])) {
            echo "<script>window.location='inbox.php'</script>";
        }
        ?>


        <div class="block">               
            <form action="" method="post" enctype="multipart/form-data">
                <?php
                $query = "SELECT * FROM tbl_contact WHERE id='$id'";
                $category = $obj->select($query);
                if ($category) {
                    foreach ($category as $value) {
                        ?>
                        <table class="form">
                            <tr>
                                <td>
                                    <label>Name</label>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="<?php echo $value['firstname'] . ' ' . $value['lastname']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Email</label>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="<?php echo $value['email']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Date</label>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="<?php echo $fm->formatDate($value['date']); ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Message Body</label>
                                </td>
                                <td>
                                    <textarea class="tinymce" readonly="">
                                        <?php echo $value['body']; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="OK" />
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                }
                ?>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>