<?php
include '../lib/session.php';
Session::checkLogin();
?>


<?php
include '../config/config.php';
include '../lib/Database.php';
include '../helpers/format.php';
?>

<?php
$obj = new Database;
$fm = new Format;
?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
    <div class="container">
        <section id="content">
            <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $username = $fm->validation($_POST['username']);
                $password = $fm->validation(md5($_POST['password']));

                //required update php virsion
                /* $username = mysqli_real_escape_string($username);
                  $password = mysqli_real_escape_string($password); */

                $query = "SELECT * FROM tbl_user WHERE username='$username' AND password='$password'";

                $result = $obj->select($query);

                if ($result != FALSE) {
                    $value = mysqli_fetch_array($result);
                    if (mysqli_num_rows($result) > 0) {
                        Session::set("login", TRUE);
                        Session::set("username", $value['username']);
                        Session::set("userId", $value['user_id']);
                        Session::set("userRole", $value['role']);
                        header('location:index.php');
                    } else {
                        echo "<span style='color:red;font-size:20px;'>no result found</span>";
                    }
                } else {
                    echo "<span style='color:red;font-size:20px;'>username/password not match</span>";
                }
            }
            ?>
            <form action="login.php" method="post">
                <h1>Admin Login</h1>
                <div>
                    <input type="text" placeholder="Username" required="" name="username"/>
                </div>
                <div>
                    <input type="password" placeholder="Password" required="" name="password"/>
                </div>
                <div>
                    <input type="submit" value="Log in" />
                </div>
            </form>
            <div class="button">
                <a href="forgotpass.php">Forgot Password</a>
            </div><!-- button -->
        </section><!-- content -->
    </div><!-- container -->
</body>
</html>