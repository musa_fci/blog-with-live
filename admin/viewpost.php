<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!isset($_GET['id']) || $_GET['id'] == NULL) {
    echo "<script>window.location='postlist.php'</script>";
} else {
    $id = $_GET['id'];
}
?>



<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Post</h2>
        <?php
        if (isset($_POST['btn'])) {
            echo "<script>window.location='postlist.php'</script>";
        }
        ?>


        <div class="block">

            <?php
            $query = "SELECT * FROM tbl_post WHERE id = '$id'";
            $post = $obj->select($query);
            if ($post) {
                foreach ($post as $postValue) {
                    ?>

                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="form">

                            <tr>
                                <td>
                                    <label>Title</label>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="<?php echo $postValue['title']; ?>" class="medium" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label>Category</label>
                                </td>
                                <td>
                                    <select id="select" readonly="">
                                        <option>select category</option>
                                        <?php
                                        $query = "SELECT * FROM tbl_category";
                                        $result = $obj->select($query);
                                        if ($result) {
                                            foreach ($result as $value) {
                                                ?>
                                                <option 
                                                <?php if ($postValue['cat_id'] == $value['category_id']) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                    value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Upload Image</label>
                                </td>
                                <td>
                                    <img src="upload/<?php echo $postValue['image']; ?>" width="150px" height="50px">
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Content</label>
                                </td>
                                <td>
                                    <textarea class="tinymce">
                                        <?php echo $postValue['body']; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Tags</label>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="<?php echo $postValue['tags']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Author</label>
                                </td>
                                <td>
                                    <input type="text" readonly="" value="<?php echo $postValue['author']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="ok" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>