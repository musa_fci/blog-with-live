<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Slider</h2>
        <?php
        if (isset($_POST['btn'])) {

            $permit = array('jpg', 'png', 'jpeg', 'gif');
            $pic_name = $_FILES['image']['name'];
            $pic_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $pic_name);
            $pic_ext = strtolower(end($div));
            $unique_name = substr(md5(time()), 0, 10) . '.' . $pic_ext;
            $pic_folder = "upload/" . $unique_name;

            if (empty($pic_name)) {
                echo 'No Image Select';
            } elseif ($pic_size >= 1048567) {
                echo 'Image Size is too Larze';
            } elseif (in_array($pic_ext, $permit) === FALSE) {
                return "select correct file formate like as:- " . implode(',', $permit);
            } else {
                move_uploaded_file($tmp_name, $pic_folder);

                $query = "INSERT INTO tbl_slider(title,image)VALUES('$_POST[title]','$unique_name')";
                $postinsert = $obj->insert($query);
                if ($postinsert) {
                    echo 'Slider Image Insert Successfully....!';
                } else {
                    echo 'Slider Image Not Inserted.....!';
                }
            }
        }
        ?>


        <div class="block">               
            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="title" placeholder="Enter Slider Title..." class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <input name="image" type="file" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="btn" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>