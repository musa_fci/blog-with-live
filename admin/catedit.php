<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!isset($_GET['catId']) || $_GET['catId'] == NULL) {
//    header('location:catlist.php');
    echo "<script>window.location='catlist.php'</script>";
} else {
    $catId = $_GET['catId'];
}
?>



<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Category</h2>
        <div class="block copyblock"> 
            <?php
            if (isset($_POST['btn'])) {
                $data = $_POST['category_name'];

                if (empty($data)) {
                    echo "Field Must not be Empty";
                } else {
                    $query = "UPDATE tbl_category SET category_name='$data' WHERE category_id='$catId' ";
                    $updated = $obj->update($query);

                    if ($updated) {
                        echo "Category Updated Successfully....!";
                    } else {
                        echo "No Category Updated";
                    }
                }
            }
            ?>

            <?php
            $query = "SELECT * FROM tbl_category WHERE category_id = '$catId'";
            $category = $obj->select($query);
            if ($category) {
                foreach ($category as $value) {
                    ?>
                    <form action="" method="post">
                        <table class="form">					
                            <tr>
                                <td>
                                    <input type="text" name="category_name" value="<?php echo $value['category_name']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr> 
                                <td>
                                    <input type="submit" name="btn" Value="update" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<?php
include './inc/footer.php';
?>
