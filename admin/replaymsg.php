<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!isset($_GET['msgid']) || $_GET['msgid'] == NULL) {
    echo "<script>window.location='inbox.php'</script>";
} else {
    $id = $_GET['msgid'];
}
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>View Messages</h2>

        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $toEmail = $fm->validation($_POST['toEmail']);
            $formEmail = $fm->validation($_POST['formEmail']);
            $subject = $fm->validation($_POST['subject']);
            $message = $fm->validation($_POST['message']);

            $sendmail = mail($toEmail, $subject, $message, $formEmail);

            if ($sendmail) {
                echo "<span style='color:green'>Mail Send Successfully......!</span>";
            } else {
                echo "<span style='color:red'>Mail Not Send...........!</span>";
            }
        }
        ?>


        <div class="block">               
            <form action="" method="post" enctype="multipart/form-data">
                <?php
                $query = "SELECT * FROM tbl_contact WHERE id='$id'";
                $category = $obj->select($query);
                if ($category) {
                    foreach ($category as $value) {
                        ?>
                        <table class="form">
                            <tr>
                                <td>
                                    <label>To</label>
                                </td>
                                <td>
                                    <input type="text" name="toEmail" readonly="" value="<?php echo $value['email']; ?>" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Form</label>
                                </td>
                                <td>
                                    <input type="text" name="formEmail" placeholder="Please Enter Your email address" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Subject</label>
                                </td>
                                <td>
                                    <input type="text" name="subject" placeholder="Please Enter Subject" class="medium" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Message Body</label>
                                </td>
                                <td>
                                    <textarea class="tinymce" name="message">
                                                                                                                                                                
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="btn" Value="send" />
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                }
                ?>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php
include './inc/footer.php';
?>