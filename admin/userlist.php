<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>User List</h2>
        <?php
        if (isset($_GET['deluser'])) {
            $deluser = $_GET['deluser'];
            $query = "DELETE FROM tbl_user WHERE user_id='$deluser'";
            $msg = $obj->delete($query);
            if ($msg) {
                echo "Usre Delete Successfuly....!";
            } else {
                echo "User Not Delete";
            }
        }
        ?>
        <div class="block">        
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Details</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT * FROM tbl_user";
                    $user = $obj->select($query);
                    $i = 0;
                    if ($user) {
                        foreach ($user as $value) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['username']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $fm->textShort($value['details'], 30); ?></td>
                                <td>
                                    <?php
                                    if ($value['role'] == '0') {
                                        echo 'Admin';
                                    } elseif ($value['role'] == '1') {
                                        echo 'Author';
                                    } elseif ($value['role'] == '2') {
                                        echo 'Editor';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="viewuser.php?userid=<?php echo $value['user_id']; ?>">View</a> 
                                    <?php
                                    if (Session::get('userRole') == '0') {
                                        ?>
                                        || <a onclick="return confirm('Are You Sure to Delete !!')" href="?deluser=<?php echo $value['user_id']; ?>">Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php
include './inc/footer.php';
?>