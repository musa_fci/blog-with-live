<?php
include './inc/header.php';
?>
<?php
include './inc/sidebar.php';
?>

<?php
if (!Session::get('userRole') == '0') {
    echo "<script>window.location='index.php'</script>";
}
?>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New User</h2>
        <div class="block copyblock"> 
            <?php
            if (isset($_POST['btn'])) {
                $username = $fm->validation($_POST['username']);
                $password = $fm->validation(md5($_POST['password']));
                $email = $fm->validation($_POST['email']);
                $role = $fm->validation($_POST['role']);


                if (empty($username) || empty($password) || empty($role) || empty($email)) {
                    echo "Field Must not be Empty";
                } else {
                    $mailquery = "SELECT * FROM tbl_user WHERE email='$email' LIMIT 1";
                    $mailcheck = $obj->select($mailquery);
                    if ($mailcheck != FALSE) {
                        echo "User/Eamil already exist....!";
                    } else {
                        $query = "INSERT INTO tbl_user (username,password,email,role)VALUES('$username','$password','$email','$role')";
                        $inserted = $obj->insert($query);

                        if ($inserted) {
                            echo "User Create Successfully....!";
                        } else {
                            echo "No User Created......!";
                        }
                    }
                }
            }
            ?>

            <form action="" method="post">
                <table class="form">					
                    <tr>
                        <td>
                            <label>User Name</label>
                        </td>
                        <td>
                            <input type="text" name="username" placeholder="Enter User Name..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Password</label>
                        </td>
                        <td>
                            <input type="password" name="password" placeholder="Enter Password..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Email</label>
                        </td>
                        <td>
                            <input type="text" name="email" placeholder="Enter Email..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>User Role</label>
                        </td>
                        <td>
                            <select id="select" name="role">
                                <option>select user role</option>
                                <option value="0">Admin</option>
                                <option value="1">Author</option>
                                <option value="2">Editor</option>
                            </select>
                        </td>
                    </tr>
                    <tr> 
                        <td></td>
                        <td>
                            <input type="submit" name="btn" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php
include './inc/footer.php';
?>
