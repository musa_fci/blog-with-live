<?php
include './inc/header.php';
?>

<?php
if (!isset($_GET['id']) || $_GET['id'] == NULL) {
    header('location:404.php');
} else {
    $id = $_GET['id'];
}
?>


<div class="contentsection contemplete clear">
    <div class="maincontent clear">
        <div class="about">
            <?php
            $query = "SELECT * FROM tbl_post WHERE id=$id";
            $post = $obj->select($query);
            if ($post) {
                foreach ($post as $data) {
                    ?>

                    <h2><?php echo $data['title']; ?></h2>
                    <h4><?php echo $fm->formatDate($data['date']); ?>, By <a href="#"><?php echo $data['author']; ?></a></h4>
                    <img src="admin/upload/<?php echo $data['image']; ?>" alt="post image"/>
                    <p>
                        <?php echo $data['body']; ?>
                    </p>



                    <div class="relatedpost clear">
                        <h2>Related articles</h2>
                        <?php
                        $catId = $data['cat_id'];
                        $queryRelated = "SELECT * FROM tbl_post WHERE cat_id='$catId' ORDER BY rand() LIMIT 6";
                        $postRelated = $obj->select($queryRelated);
                        if ($postRelated) {
                            foreach ($postRelated as $data) {
                                ?>
                                <a href="post.php?id=<?php echo $data['id']; ?>"><img src="admin/upload/<?php echo $data['image']; ?>" alt="post image"/></a>
                                <?php
                            }
                        } else {
                            echo 'No Related Post';
                        }
                        ?>

                    </div>



                    <?php
                }
            } else {
                header('location:404.php');
            }
            ?>
        </div>

    </div>
    <?php
    include './inc/sidebar.php';
    ?> 
</div>

<?php
include './inc/footer.php';
?>