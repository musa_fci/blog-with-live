<?php
include './inc/header.php';
include './inc/slider.php';
?>

<?php
if (!isset($_GET['category_id']) || $_GET['category_id'] == NULL) {
    header('location:404.php');
} else {
    $category_id = $_GET['category_id'];
}
?>

<div class="contentsection contemplete clear">

    <div class="maincontent clear">

        <?php
        $query = "SELECT * FROM tbl_post WHERE cat_id='$category_id'";
        $post = $obj->select($query);

        if ($post) {
            foreach ($post as $data) {
                ?>
                <div class="samepost clear">
                    <h2>
                        <a href="post.php?id=<?php echo $data['id']; ?>">
                            <?php echo $data['title']; ?>
                        </a>
                    </h2>
                    <h4><?php echo $fm->formatDate($data['date']); ?>, By <a href="#"><?php echo $data['author']; ?></a></h4>
                    <a href="#"><img src="admin/upload/<?php echo $data['image']; ?>" alt="post image"/></a>
                    <p>
                        <?php echo $fm->textShort($data['body'], 350); ?>
                    </p>
                    <div class="readmore clear">
                        <a href="post.php?id=<?php echo $data['id']; ?>">Read More</a>
                    </div>
                </div>
                <?php
            }
        } else {
            header('location:404.php');
        }
        ?>

    </div>



</div>
<?php
include './inc/sidebar.php';
?>

<?php
include './inc/footer.php';
