<?php
include './config/config.php';
include './lib/Database.php';
include './helpers/format.php';
?>

<?php
$obj = new Database;
$fm = new Format;
?>

<!DOCTYPE html>
<html>
    <head>

        <?php include './scripts/meta.php'; ?>
        <?php include './scripts/css.php'; ?>
        <?php include './scripts/js.php'; ?>
    </head>

    <body>
        <div class="headersection templete clear">
            <a href="#">
                <?php
                $query = "SELECT * FROM title_slogan WHERE id='1'";
                $data = $obj->select($query);
                if ($data) {
                    foreach ($data as $value) {
                        ?>
                        <div class="logo">
                            <img src="admin/upload/<?php echo $value['logo']; ?>" alt="Logo"/>
                            <h2><?php echo $value['title']; ?></h2>
                            <p><?php echo $value['slogan']; ?></p>
                        </div>
                        <?php
                    }
                }
                ?>
            </a>
            <div class="social clear">
                <?php
                $query = "SELECT * FROM tbl_social WHERE id='1'";
                $data = $obj->select($query);
                if ($data) {
                    foreach ($data as $value) {
                        ?>
                        <div class="icon clear">
                            <a href="<?php echo $value['fb']; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo $value['tw']; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo $value['ln']; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="<?php echo $value['gp']; ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                        </div>
                        <?php
                    }
                }
                ?>
                <div class="searchbtn clear">
                    <form action="search.php" method="post">
                        <input type="text" name="search" placeholder="Search keyword..."/>
                        <input type="submit" name="submit" value="Search"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="navsection templete">
            <?php
            $path = $_SERVER['SCRIPT_FILENAME'];
            $cpage = basename($path, '.php');
            ?>
            <ul>
                <li>
                    <a 
                    <?php
                    if ($cpage == 'index') {
                        echo 'id="active"';
                    }
                    ?>
                        href="index.php">Home</a>
                </li>
                <?php
                $query = "SELECT * FROM tbl_page";
                $result = $obj->select($query);
                if ($result) {
                    foreach ($result as $value) {
                        ?>
                        <li>
                            <a 
                            <?php
                            if (isset($_GET['pageid']) && $_GET['pageid'] == $value['id']) {
                                echo 'id="active"';
                            }
                            ?>
                                href="page.php?pageid=<?php echo $value['id']; ?>">
                                    <?php echo $value['name']; ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>	
                <li>
                    <a 
                    <?php
                    if ($cpage == 'contact') {
                        echo 'id="active"';
                    }
                    ?>
                        href="contact.php">Contact</a>
                </li>
            </ul>
        </div>
