<div class="sidebar clear">
    <div class="samesidebar clear">
        <h2>Categories</h2>
        <ul>
            <?php
            $query = "SELECT * FROM tbl_category";
            $category = $obj->select($query);

            if ($category) {
                foreach ($category as $data) {
                    ?>

                    <li>
                        <a href="posts.php?category_id=<?php echo $data['category_id']; ?>"><?php echo $data['category_name']; ?></a>
                    </li>

                    <?php
                }
            } else {
                ?>

                <li>No Category Added</li>

            <?php } ?>
        </ul>
    </div>

    <div class="samesidebar clear">
        <h2>Latest articles</h2>
        <?php
        $query = "SELECT * FROM tbl_post LIMIT 5";
        $post = $obj->select($query);

        if ($post) {
            foreach ($post as $data) {
                ?>
                <div class="popular clear">
                    <h3>
                        <a href="post.php?id=<?php echo $data['id']; ?>">
                            <?php echo $data['title']; ?>
                        </a>
                    </h3>
                    <a href="post.php?id=<?php echo $data['id']; ?>">
                        <img src="admin/upload/<?php echo $data['image']; ?>" alt="post image"/>
                    </a>
                    <p>
                        <?php echo $fm->textShort($data['body'], 120); ?>
                    </p>	
                </div>
                <?php
            }
        } else {
            header('location:404.php');
        }
        ?>


    </div>

</div>