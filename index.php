<?php
include './inc/header.php';
include './inc/slider.php';
?>

<div class="contentsection contemplete clear">

    <div class="maincontent clear">

        <!--pagination start-->
        <?php
        $per_page = 2;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $start_form = ($page - 1) * $per_page;
        ?>
        <!--pagination close-->

        <?php
        $query = "SELECT * FROM tbl_post LIMIT $start_form,$per_page";
        $post = $obj->select($query);

        if ($post) {
            foreach ($post as $data) {
                ?>

                <div class="samepost clear">
                    <h2>
                        <a href="post.php?id=<?php echo $data['id']; ?>">
                            <?php echo $data['title']; ?>
                        </a>
                    </h2>
                    <h4><?php echo $fm->formatDate($data['date']); ?>, By <a href="#"><?php echo $data['author']; ?></a></h4>
                    <a href="#"><img src="admin/upload/<?php echo $data['image']; ?>" alt="post image"/></a>
                    <p>
                        <?php echo $fm->textShort($data['body'], 350); ?>
                    </p>
                    <div class="readmore clear">
                        <a href="post.php?id=<?php echo $data['id']; ?>">Read More</a>
                    </div>
                </div>

            <?php } ?>

            <!--pagination start-->
            <?php
            $query = "SELECT * FROM tbl_post";
            $post = $obj->select($query);
            $total_rows = mysqli_num_rows($post);
            $total_pages = ceil($total_rows / $per_page);

            echo "<span class='pagination'><a href='index.php?page=1'>" . 'first page' . "</a>";

            for ($i = 1; $i <= $total_pages; $i++) {
                echo "<a href='index.php?page=" . $i . "'>" . $i . "</a>";
            }

            echo "<a href='index.php?page=$total_pages'>" . 'last page' . "</a></span>";
            ?>
            <!--pagination close-->

            <?php
        } else {
            header('location:404.php');
        }
        ?>








    </div>

    <?php
    include './inc/sidebar.php';
    ?>

</div>

<?php
include './inc/footer.php';
?>